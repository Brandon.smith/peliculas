import React, { useEffect, useState } from 'react';
import { Grid, Typography } from '@mui/material';
import Clases from '../componentes/Clases';

const matematicas = [
  {
    "id": 1,
    "first_name": "Cass",
    "last_name": "Drakers",
    "sexo": "Female",
    "foto": "https://robohash.org/minimadoloresratione.png?size=50x50&set=set1"
  },
  {
    "id": 2,
    "first_name": "Midge",
    "last_name": "Waples",
    "sexo": "Female",
    "foto": "https://robohash.org/aetqui.png?size=50x50&set=set1"
  },
  {
    "id": 3,
    "first_name": "Tommy",
    "last_name": "Braznell",
    "sexo": "Female",
    "foto": "https://robohash.org/quibusdamenimsuscipit.png?size=50x50&set=set1"
  },
  {
    "id": 4,
    "first_name": "Dillon",
    "last_name": "Sholem",
    "sexo": "Male",
    "foto": "https://robohash.org/consecteturnesciuntalias.png?size=50x50&set=set1"
  },
  {
    "id": 5,
    "first_name": "Layney",
    "last_name": "Hallin",
    "sexo": "Female",
    "foto": "https://robohash.org/quieosprovident.png?size=50x50&set=set1"
  },
  {
    "id": 6,
    "first_name": "Lilia",
    "last_name": "Danielovitch",
    "sexo": "Female",
    "foto": "https://robohash.org/dolorereiciendissit.png?size=50x50&set=set1"
  },
  {
    "id": 7,
    "first_name": "Breena",
    "last_name": "Kaindl",
    "sexo": "Bigender",
    "foto": "https://robohash.org/laboruminet.png?size=50x50&set=set1"
  },
  {
    "id": 8,
    "first_name": "Chrissy",
    "last_name": "McKenney",
    "sexo": "Bigender",
    "foto": "https://robohash.org/velitaccusamussunt.png?size=50x50&set=set1"
  },
  {
    "id": 9,
    "first_name": "Mendy",
    "last_name": "Brownsall",
    "sexo": "Male",
    "foto": "https://robohash.org/odiovelitminima.png?size=50x50&set=set1"
  },
  {
    "id": 10,
    "first_name": "Filberto",
    "last_name": "Dincke",
    "sexo": "Male",
    "foto": "https://robohash.org/magnamquiquae.png?size=50x50&set=set1"
  }
];

const ciencias = [{
  "id": 1,
  "first_name": "Valina",
  "last_name": "McCarry",
  "sexo": "Bigender",
  "foto": "https://robohash.org/asperioresrerumvoluptatem.png?size=50x50&set=set1"
}, {
  "id": 2,
  "first_name": "Christophe",
  "last_name": "Cocksedge",
  "sexo": "Male",
  "foto": "https://robohash.org/harumminusut.png?size=50x50&set=set1"
}, {
  "id": 3,
  "first_name": "Emlyn",
  "last_name": "Slides",
  "sexo": "Female",
  "foto": "https://robohash.org/excepturiexplicabonatus.png?size=50x50&set=set1"
}, {
  "id": 4,
  "first_name": "Eadith",
  "last_name": "Raleston",
  "sexo": "Female",
  "foto": "https://robohash.org/repellattotamest.png?size=50x50&set=set1"
}, {
  "id": 5,
  "first_name": "Raye",
  "last_name": "Melendez",
  "sexo": "Female",
  "foto": "https://robohash.org/eaquequidistinctio.png?size=50x50&set=set1"
}, {
  "id": 6,
  "first_name": "Danyelle",
  "last_name": "Ivanyutin",
  "sexo": "Female",
  "foto": "https://robohash.org/placeatsintreprehenderit.png?size=50x50&set=set1"
}, {
  "id": 7,
  "first_name": "Garrick",
  "last_name": "Abilowitz",
  "sexo": "Male",
  "foto": "https://robohash.org/commodiatsit.png?size=50x50&set=set1"
}, {
  "id": 8,
  "first_name": "Mendy",
  "last_name": "Cockran",
  "sexo": "Male",
  "foto": "https://robohash.org/inestalias.png?size=50x50&set=set1"
}, {
  "id": 9,
  "first_name": "Hersch",
  "last_name": "Agget",
  "sexo": "Male",
  "foto": "https://robohash.org/velitnonlaudantium.png?size=50x50&set=set1"
}, {
  "id": 10,
  "first_name": "Claudell",
  "last_name": "Chang",
  "sexo": "Male",
  "foto": "https://robohash.org/voluptatemutatque.png?size=50x50&set=set1"
}];

const index = () => {
  const [matematica, setMatematica] = useState([]);

  useEffect(() => {
    setMatematica(matematicas);
  }, [matematica])

  return (
    <Grid>
      <Typography variant='h3'>Cursos del Plan</Typography>
      <Clases titulo={'Matematicas'} matematica={matematica} />
      <Clases titulo={'Ciencias'} ciencias={ciencias} />
    </Grid>
  )
}

export default index;
